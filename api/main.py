

from fastapi import FastAPI
from router.routes import router

app = FastAPI()
# OPTIONAL SETTINGS
#  app = FastAPI(title=settings.PROJECT_NAME,version=settings.PROJECT_VERSION)

app.include_router(router)
